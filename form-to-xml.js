
/**
 * Генерируем ссылку на blob элемент указявая содержимое файла используя строку
 *
 * @param {string} xmlString - Xml content
 */
function getBlobUrlXml (xmlString)
{
    // Создаем BLOB и пишем строку в него
    var blob  = new Blob(['<?xml version="1.0"?>', xmlString], {type: "text/xml"});

    // Получаем URL с помощью созданного BLOB
    return window.URL.createObjectURL(blob);
}

/**
 * Генерируем XML строку
 *
 * @param {object} object
 * @param {string} root - Root element for xml
 */
function getXmlStringFromObject(object, root)
{
    // Устанавливаем дефолтное имя root элемента
    root = root || "root";

    // Создаем пустую строку ее и будем
    // возвращать и добавлять в нее элементы
    var string = "";

    // Проверяем object что его тип object
    if(typeof object == "object")
    {
        // Получаем список ключей обьекта
        var keys = Object.keys(object);

        // Перебираем ключи
        for(var i = 0; i < keys.length; i++)
        {
            // Открываем xml тэг
            string += "<" + keys[i] + ">";

            // Добавляем значение
            string += object[keys[i]];

            // Закрываем xml тэг
            string += "</" + keys[i] + ">";
        }
    }

    // Добавляем root элемент и возвращаем строку
    return "<" + root + ">" + string + "</" + root + ">";
}

/**
 * Получить обьект значений элементов формы
 *
 * @param form - form DOM element
 * @return object - form data object
 */
function getFormDataObject(form)
{
    // Создаем пустое значение
    var data = {};

    // Перебираем элементы формы
    for(var i = 0; i < form.elements.length; i++)
    {
        // Проверяем имя элемента и значение элемента
        if(form.elements[i].name && form.elements[i].value)
        {
            // Добавляем проперти в data
            data[form.elements[i].name] = form.elements[i].value;
        }

    }

    // Возвращаю дату
    return data;
}

